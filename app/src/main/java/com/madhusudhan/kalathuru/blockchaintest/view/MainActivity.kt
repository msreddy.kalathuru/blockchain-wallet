package com.madhusudhan.kalathuru.blockchaintest.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.madhusudhan.kalathuru.blockchaintest.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}